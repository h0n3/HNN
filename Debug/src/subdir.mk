################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DataSet.cpp \
../src/Network.cpp \
../src/Neuron.cpp \
../src/Training.cpp 

OBJS += \
./src/DataSet.o \
./src/Network.o \
./src/Neuron.o \
./src/Training.o 

CPP_DEPS += \
./src/DataSet.d \
./src/Network.d \
./src/Neuron.d \
./src/Training.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



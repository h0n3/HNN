/*****************************
 * @file	DataDef.h
 * @date	Jun 17, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef DATADEF_H_
#define DATADEF_H_

#include <vector>
#include <valarray>

class Neuron;

// Network
using Layer = std::vector<Neuron*>;
using Net = std::vector<Layer>;
using vec = std::valarray<double>;

//Neuron
using actFunc = double(*)(double);
using weightLayer = std::valarray<double>; // TODO valarray

#endif /* DATADEF_H_ */

/*****************************
 * @file	DataSet.cpp
 * @date	Jun 17, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "DataSet.h"



DataSet::DataSet(std::size_t in, std::size_t out)
:_numInputs{in}, _numOutputs{out}{

}

DataSet::~DataSet() {
	// TODO Auto-generated destructor stub
}

void DataSet::addCase(TestCase& testCase) {
	if (testCase._in.size() != _numInputs) throw "tried to add TestCase with wrong number of Inputs";
	if (testCase._out.size() != _numOutputs) throw "tried to add TestCase with wrong number of Outputs";

	_data.push_back(testCase);
}

void DataSet::addCase(TestCase&& testCase) {
	if (testCase._in.size() != _numInputs) throw "tried to add TestCase with wrong number of Inputs";
	if (testCase._out.size() != _numOutputs) throw "tried to add TestCase with wrong number of Outputs";

	_data.push_back(testCase);
}

void DataSet::addCase(vec in, vec out) {
	addCase(TestCase(std::move(in),std::move(out)));
}

void DataSet::randomlyDistributeTestCases(double learn, double validate,double test) {
	double sum = learn + validate + test;
	if  ((sum > 1.1) || (sum < 0.9)) throw "distribution does not add up to 100%";

	const std::size_t numCases = _data.size();

	// Calc how many of each type is needed
	std::size_t numTest  = numCases * test;
	std::size_t numValid = numCases * validate;
	std::size_t numLearn = numCases * learn;

	// ensure that we can assing every case
	while ((numTest + numValid + numLearn) < numCases) ++numLearn;
	while ((numTest + numValid + numLearn) > numCases) --numLearn;

	std::cout << "Randomly Distributing " << numCases << " TestCases: " << numTest << "test | " << numValid << " validation | " << numLearn << "learning" << std::endl;

	std::size_t actTest = 0;
	std::size_t actValid = 0;
	std::size_t actLearn = 0;

	double val_threshold = learn + validate;

	std::random_device rd;
	std::mt19937 rand_dev{rd()};

	std::uniform_real_distribution<double> dist(0.0, 1.0);

	for (auto& t_case : _data){
		double dice = dist(rand_dev);
		if ((dice < learn)&&(actLearn <= numLearn )){
			// randomly choose this
			t_case.type = TestCase::Type::learn;
			++actLearn;
		} else if ((dice < val_threshold)&&(actValid <= numValid)){
			t_case.type = TestCase::Type::validate;
			++actValid;

		} else if(actTest <= numTest){
			t_case.type = TestCase::Type::test;
			++actTest;
		} else {
			if (actLearn <= numLearn ){
				t_case.type = TestCase::Type::learn;
				++actLearn;
			} else if (actValid <= numValid){ // should be done with else only TODO check logic
				t_case.type = TestCase::Type::validate;
				++actValid;
			}

		}
	}

	std::cout << "assigned: " << actTest <<  " TestCases | " << actLearn << " LearningCases | " << actValid << " ValidationCases" << std::endl;
}

double DataSet::calcRMS(Net& net) {

	for (auto& t_case : _data){

	}
}

double DataSet::calcRMSTest(Net& net) {
}

/*
std::string convertCSVtoXML(char* in, char lineDelim, char* tokenDelim, char* out){
	std::ifstream if_in{};

	if_in.open(in);


	std::cout << "Opening File:\"" << in << "\" Status:" <<  if_in.is_open()  << " Size:";

	int size = GetFileSize(in);

	std::cout << size << std::endl;


	if_in.seekg (0, std::ios::beg);

	std::vector<std::string> lines;

	// Get Data Names
	std::string line;
	std::getline(if_in,line);

	// Read name tokens into this name vec
	//std::vector<std::string> names;
	OpenNN::Vector<std::string> names;
	OpenNN::Vector<OpenNN::Variables::Use> uses;

	char* act_line = new char[line.size()+1];
	memcpy(act_line,line.c_str(),line.size()+1);

	char* token = strtok(act_line,tokenDelim);

	while(token != NULL){
		names.emplace_back(token);
#ifdef NO_GUI
		std::cout << "Found Coloum with Name \"" << token << "\" set Input Mode: 0=IN 1=OUT 2=NUTTIN" << std::endl;
		int use;
		std::cin >> use;

		switch (use){
		case 0:{
			std::cout << " Set mode to: INPUT" << std::endl;
			uses.push_back(OpenNN::Variables::Use::Input);
			break;
		}
		case 1:{
			std::cout << " Set mode to: TARGET" << std::endl;
			uses.push_back(OpenNN::Variables::Use::Target);
			break;
		}
		case 2:{
			std::cout << " Set mode to: UNUSED" << std::endl;
			uses.push_back(OpenNN::Variables::Use::Unused);
			break;
		}
		default:{
			std::cout << " Set mode to: UNUSED" << std::endl;
			uses.push_back(OpenNN::Variables::Use::Unused);
		}
		}

#endif
		token = strtok(NULL,tokenDelim);
	}

	int colums = names.size();

	std::cout << "Got " << colums << " Different colums: \"";

	for (auto name : names){
		std::cout << name << " | ";
	}
	std::cout << "\"" << std::endl;

	delete[] act_line;


	// Start reading rows
	while(std::getline(if_in,line,lineDelim)){
		lines.push_back(line);

		//std::cout << "Reading input Line: " << line << std::endl;
	}

	int rows = lines.size();

	std::cout << "Read a total of " << rows << " Lines" << std::endl;
	if_in.close();

	// Create Matzrix to hold training data
	OpenNN::Matrix<double> data(rows,colums);
	int row_count = 0;

	//Read the input data into the matrix
	for (auto row: lines)
	{
		char* act_row = new char[row.size()+1];
		memcpy(act_row,row.c_str(),row.size()+1);

		OpenNN::Vector<double> row_data(colums);

		int token_count = 0;

		char* token = strtok(act_row, tokenDelim);
		while(token != NULL){
			if (token_count == colums) throw "Sth super wrong here";

			row_data[token_count++] = strtod(token,NULL);

			token = strtok(NULL,tokenDelim);
		}

		data.set_row(row_count++,row_data);

		delete[] act_row;
	}
	OpenNN::DataSet set = OpenNN::DataSet{data};

	OpenNN::Variables* vars = set.get_variables_pointer();

	vars->set_names(names);
	vars->set_uses(uses);

	//out.print_summary();

	//set.print();


	std::cout << "Reading Data Complete" << std::endl;

	// Save
	std::string out_name;
	if (out == NULL)
	{
		std::string out_name(strtok(in,"."));
		out_name += ".xml";

	} else {
		out_name = out;
	}
	std::cout << "Trying to save file at location \"" << out_name << "\"" << std::endl;

	set.save(out_name);

	return out_name;
}
 */

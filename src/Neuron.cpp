/*****************************
 * @file	Neuron.cpp
 * @date	Jun 15, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "Neuron.h"

#define DEBUG


double IdentityActivation(double in){
	return in; // damn this func is complex af
}
double IdentityActivationDerivate(double in){
	return 1.0;
}

double LogisticActivation(double in){
	return 1.0 / (1.0 + std::exp(-in));
}
double LogisticActivationDerivate(double in){
	register double f_x = LogisticActivation(in);
	return f_x * (1.0 - f_x);
}


Neuron::Neuron(size_t pre, ActivationFuncs activationFunction)
:_weights(pre),_actFunc{activationFunction}
{

}

Neuron::~Neuron() {
	// TODO Auto-generated destructor stub
}

void Neuron::initWeightsUniform(double val, std::size_t num) {
	if (num) _weights = std::valarray<double>(num);
	for (double& w : _weights){
		w = val;
	}
}

void Neuron::initWeightsRandom(double low, double high, std::size_t num){
	std::random_device rd;
	std::mt19937 rand_dev{rd()};

	std::uniform_real_distribution<double> dist(low, high);

	if (num) _weights = std::valarray<double>(num);

	for (double& w : _weights){
		w = dist(rand_dev);
	}
#ifdef DEBUG
	std::cout << "Randomly initialized Weights of a neuron to[";
	for (auto& w : _weights){
		std::cout << w << "|";
	}
	std::cout << "]" << std::endl;
#endif

}

double Neuron::getOut(double net){
	return getActivationFunction()(net);
}



double Neuron::getNet(std::valarray<double>& in){
	std::valarray<double> result = (in *_weights);
	return result.sum();
}

weightLayer& Neuron::getWeights(){
	return _weights;
}

void Neuron::setWeights(weightLayer&& w) {
	_weights = w; // ??? std::move(w) should be redundant since w is already a copy reference(&&)
}

double Neuron::getWeight(std::size_t at) {
	return _weights[at];
}

void Neuron::setWeigth(std::size_t at, double weigth){
	_weights[at] = weigth;
}

void Neuron::addToWeight(std::size_t at, double diff) {
	_weights[at] += diff;
}



std::string getActFuncDesc(ActivationFuncs a){
	switch(a){
	case ActivationFuncs::Logistic:{
		return "Logistic";
	}
	case ActivationFuncs::Identity:{
			return "Identity";
		}
	default:{
		throw "requesting desc for act_func type that is not defined";
	}
	}
}

bool isValidActivationFunction(ActivationFuncs a){
	return true;
}

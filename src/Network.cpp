/*****************************
 * @file	Network.cpp
 * @date	Jun 15, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#include "Network.h"


//#define DEBUG

Network::Network()
:_num_inputs{0},_lblIn{},_lblOut{},_hiddenLayer{},_outputLayer{}
{
}

void Network::setSizeOfInputLayer(std::size_t size) {
	_num_inputs = size;
}

Network::Network(std::size_t inputs, std::size_t outputs, std::size_t hiddenLayer, std::size_t sizeLayer, bool connect){

	_num_inputs = inputs;

	//Create hidden layer
	_hiddenLayer = std::vector<Layer>(hiddenLayer);

	for (std::size_t layer = 0; layer < hiddenLayer; ++layer){

		_hiddenLayer[layer] = Layer(sizeLayer);
		Layer& actLayer = _hiddenLayer[layer];

		for (std::size_t neuron = 0; neuron < sizeLayer; ++neuron){
			if (connect){
				if (layer){
					actLayer[neuron] = new Neuron(sizeLayer);
				} else {
					actLayer[neuron] = new Neuron(inputs);
				}
			} else {
				actLayer[neuron] = new Neuron(0);
			}
		}
	}

	//Create output layer
	_outputLayer = Layer(outputs);

	for (std::size_t output = 0; output < outputs; ++output){
		if (connect){
			_outputLayer[output] = new Neuron(sizeLayer);
		} else{
			_outputLayer[output] = new Neuron(0);
		}
	}

}

Network::~Network() {
	// Delete hidden Layer
	for (auto& layer : _hiddenLayer){
		for (Neuron* n : layer){
			delete n;
		}
	}
	// Delete output layer
	for (Neuron* n : _outputLayer){
		delete n;
	}
}

std::size_t Network::getSizeOfInputLayer() const{
	return _num_inputs;
}
inline std::size_t Network::getNumHiddenLayers() const{
	return _hiddenLayer.size();
}
std::size_t Network::getSizeOfHiddenLayer(std::size_t layer) const{
	return _hiddenLayer.at(layer).size();
}
std::size_t Network::getSizeOfOutputLayer() const{
	return _outputLayer.size();
}

void Network::initAllWeightsRandom(double low, double high){
	for (auto n : _outputLayer){
		n->initWeightsRandom(low,high);
	}
	for (auto& l : _hiddenLayer){
		for (auto n : l){
			n->initWeightsRandom(low,high);
		}
	}
}
void Network::initAllWeightsUniform(double init){
	for (auto n : _outputLayer){
		n->initWeightsUniform(init);
	}
	for (auto& l : _hiddenLayer){
		for (auto n : l){
			n->initWeightsUniform(init);
		}
	}

}

Neuron* Network::getHiddenNeuron(std::size_t layer, std::size_t neuron){
	return _hiddenLayer.at(layer).at(neuron);
}
void Network::setHiddenNeuron(std::size_t layer, std::size_t neuron, Neuron* n){
	_hiddenLayer.at(layer).at(neuron) = n;
}

Neuron* Network::getOutputNeuron(std::size_t neuron){
	return _outputLayer.at(neuron);
}
void Network::setOutputNeuron(std::size_t neuron, Neuron* n){
	_outputLayer.at(neuron) = n;
}

// Access
weightLayer& Network::getHiddenWeigths(std::size_t layer, std::size_t neuron){
	return _hiddenLayer.at(layer).at(neuron)->getWeights();

}
void Network::setHiddenWeights(std::size_t layer, std::size_t neuron, weightLayer&& weights){
	_hiddenLayer.at(layer).at(neuron)->setWeights(std::move(weights));
}

weightLayer& Network::getOutputWeigths(std::size_t neuron){
	return _outputLayer.at(neuron)->getWeights();
}
void Network::setOutputWeights(std::size_t neuron, weightLayer&& weights){
	_outputLayer.at(neuron)->setWeights(std::move(weights));
}

std::vector<Network::TrainingDataLayer> Network::calculateTraining(vec& in){
#ifdef DEBUG
	std::cout << "Network::calculate() called" << std::endl;
#endif
	// Allocate TrainigData
	std::vector<TrainingDataLayer> trainingsData(getNumHiddenLayers() + 1,TrainingDataLayer());

	//Calculate hidden Layer
	const std::size_t numLayer = getNumHiddenLayers();

#ifdef DEBUG
	std::cout << numLayer << " Layers to claculate" << std::endl;
#endif

	for (std::size_t layer = 0; layer < numLayer; ++layer){

		const Layer& actLayer = _hiddenLayer[layer];
		const std::size_t sizeLayer = actLayer.size();

		std::valarray<double>netWSum(0.0,sizeLayer);//    = dataLayer.net;
		std::valarray<double>activation(0.0,sizeLayer);// = dataLayer.act;

		for (std::size_t neuron = 0; neuron < sizeLayer; ++neuron){

			register double NetWeigthedSum;

			if (layer){ // hidden
				NetWeigthedSum = actLayer[neuron]->getNet(trainingsData[layer-1].act);
			} else { // input
				NetWeigthedSum = actLayer[neuron]->getNet(in);
			}


			netWSum[neuron] = NetWeigthedSum;
			activation[neuron] = actLayer[neuron]->getOut(NetWeigthedSum);

			if (std::isnan(netWSum[neuron])) throw "found the nan in hidden layer net";
			if (std::isnan(activation[neuron])) throw "found the nan in hidden layer act";

		}
		TrainingDataLayer dataLayer;
		dataLayer.net = std::move(netWSum);
		dataLayer.act = std::move(activation);

		trainingsData[layer] = dataLayer;

	}
	// Calculate outputlayer
	const std::size_t numOutputNeurons = getSizeOfOutputLayer();

	trainingsData[numLayer] = TrainingDataLayer();

	std::valarray<double>netWSum(0.0,numOutputNeurons);//    = dataLayer.net;
	std::valarray<double>activation(0.0,numOutputNeurons);// = dataLayer.act;


	for (std::size_t neuron = 0; neuron < numOutputNeurons; ++neuron){
		register double NetWeigthedSum = _outputLayer[neuron]->getNet(trainingsData[numLayer - 1].act);

		netWSum[neuron] = NetWeigthedSum;
		activation[neuron] = _outputLayer[neuron]->getOut(NetWeigthedSum);

		if (std::isnan(netWSum[neuron])) throw "found the nan in output net";
		if (std::isnan(activation[neuron])) throw "found the nan output act";

	}

	TrainingDataLayer dataLayer;
	dataLayer.net = std::move(netWSum);
	dataLayer.act = std::move(activation);

	trainingsData[numLayer] = dataLayer;

	return trainingsData;
}


vec Network::calculate(vec in){

}



void Network::setHiddenLayer(Layer&& layer, std::size_t at) {
	auto sth = _hiddenLayer.begin();
	if ((at)&&((sth + at) < _hiddenLayer.end())){
		_hiddenLayer.insert(sth,layer);
	} else {
		_hiddenLayer.push_back(layer);
	}
}

void Network::setHiddenLayer(std::size_t at, size_t num_neurons,
		const Neuron& bluePrint) {
	Layer& actLayer = _hiddenLayer.at(at);
	cleanLayer(actLayer);
	for (std::size_t neuron = 0; neuron  < num_neurons; ++neuron){
		actLayer.push_back(new Neuron(bluePrint));
	}
}

void Network::addHiddenLayer(Layer&& layer) {
	_hiddenLayer.push_back(layer);


}

void Network::addHiddenLayer(size_t num_neurons, const Neuron& bluePrint) {
	_hiddenLayer.emplace_back();
	Layer& actLayer =*(_hiddenLayer.end()-1);

	for (std::size_t neuron; neuron < num_neurons; ++neuron){
		actLayer.push_back(new Neuron(bluePrint));
	}
}

void Network::setOutputLayer(Layer&& layer) {
	cleanLayer(_outputLayer);
	_outputLayer =  layer;
}

void Network::setLabelsIn(std::vector<std::string>&& lbls) {
	if (lbls.size() != _num_inputs) throw ("Tried to add a lbl list with to many items");
	_lblIn = std::move(lbls);
}

void Network::setLabelsOut(std::vector<std::string>&& lbls) {
	if (lbls.size()  != _outputLayer.size())throw("Tried to add a lbl list with to many items");
	_lblOut = std::move(lbls);
}

void Network::setLabelIn(std::size_t at, std::string lbl) {
	_lblIn.at(at) = lbl;
}

void Network::setLabelOut(std::size_t at, std::string lbl) {
	_lblOut.at(at) = lbl;
}

std::string Network::getLabelIn(std::size_t at) const{
	return _lblIn.at(at);
}

std::string Network::getLabelOut(std::size_t at) const{
	return _lblOut.at(at);
}


double calcAbsDiff(vec v1, vec v2){
	if (v1.size() != v2.size()) throw "Tried to compare Vecs with different sizes";
	double diff = 0.0;
	for (std::size_t at = 0; at < v1.size(); ++at){
		diff += std::abs(v1[at] - v2[at]);
	}
	return diff;
}

double calcAbsSqDiff(vec v1, vec v2){
	if (v1.size() != v2.size()) throw "Tried to compare Vecs with different sizes";
	double diff = 0.0;
	for (std::size_t at = 0; at < v1.size(); ++at){
		diff += std::pow(v1[at] - v2[at],2.0);
	}
	return diff;
}



vec trainBackpropSample(Network& net, vec in, vec expected, double learningRate){

#ifdef DEBUG
	std::cout << "Training Backprop:" << std::endl;
	std::cout << "Input: [" ;
	for (auto v : in ){
		std::cout << v << "|";
	}
	std::cout << "]" << std::endl;
	std::cout << "Expected: [" ;
	for (auto v : expected ){
		std::cout << v << "|";
	}
	std::cout << "]" ;//(<< std::endl;
#endif

	// CALC
	std::vector<Network::TrainingDataLayer> data {std::move(net.calculateTraining(in))};

	// TRAIN  (act == actual, Act == Activation)
	std::size_t numOutputs = net.getSizeOfOutputLayer();
	std::size_t numHiddenLayers = net.getNumHiddenLayers();

	std::size_t size_actHiddenLayer = net.getSizeOfHiddenLayer(numHiddenLayers-1);

	vec deltas(numOutputs, 0.0);
	//deltas.resize(numOutputs,0.0); // TODO #UGLY AF

#ifdef DEBUG
	std::cout << "size exp:" << expected.size() << " size data.back:" << data.back().act.size() << std::endl;
#endif
	vec diff_vec = expected - data.back().act;

#ifdef DEBUG
	std::cout << "diff_vec: [" ;
	for (auto v : diff_vec ){
		std::cout << v << "|";
	}
	std::cout << "]" << std::endl;
#endif

#ifdef DEBUG
	std::cout << "Training OutputLayer"<< std::endl;
#endif
	// Delta Train Output Layer
	for (std::size_t oneuron = 0; oneuron < numOutputs; ++oneuron){

		Neuron& act_neuron = *(net._outputLayer[oneuron]);

		// Calc derivate of net in
		double netVal = data.back().net[oneuron];
		double derivateNet = act_neuron.getActivationFunctionDerivate()(netVal);

		//if (derivateNet != 1.0) throw "sopmehow the derivation is wrong af :D";

		// calc delta
		register double delta = derivateNet * diff_vec[oneuron];
		deltas[oneuron] = delta;

#ifdef DEBUG
	std::cout << "	Training outputneuron:" << oneuron << " with delta:" << delta << std::endl;
#endif

		std::valarray<double>& actLayerAct = data[numHiddenLayers-1].act;

		// change weights according to rule diffWeigth = learning rate * delta * output(of the neuron the weight being changed is connected to)
		for (std::size_t weigth = 0; weigth<size_actHiddenLayer; ++weigth){
			//double connectionWeight = actLayerAct[weigth];
			double connectionWeight =  data[numHiddenLayers-1].act[weigth];
			double diffWeigth = learningRate  * delta * connectionWeight;

			act_neuron.addToWeight(weigth, diffWeigth);
#ifdef DEBUG
	std::cout << "		Changed weigth by:" << diffWeigth << " from last hidden layer neuron: " << weigth <<" to outputlayer neuron:" << oneuron << std::endl;
#endif
		}
	}

#ifdef DEBUG
	std::cout << "Training HiddenLayers"<< std::endl;
#endif
	// Backpropagate the error: iterate through the hidden layers
	for (std::size_t layer = numHiddenLayers-1; layer > 0; --layer){


		size_actHiddenLayer = net.getSizeOfHiddenLayer(layer);

		Layer& actLayer = net._hiddenLayer[layer];
		Network::TrainingDataLayer& actDataLayer = data[layer];

		std::valarray<double> new_deltas(size_actHiddenLayer);

#ifdef DEBUG
	std::cout << "	Training HiddenLayer:" << layer << " with " << size_actHiddenLayer << " neurons" << std::endl;
#endif
		// iterate through the neurons of this layer
		for (std::size_t neuron = 0; neuron < size_actHiddenLayer; ++neuron){

			Neuron& actNeuron = *(actLayer[neuron]);

			double netVal = actDataLayer.net[neuron];
			double derivateNet = actNeuron.getActivationFunctionDerivate()(netVal);

			//Calc weigthed sum of deltas
			//iterate through the successor layer
			Layer* sucLayerPTR = nullptr;
			std::size_t sucNumNeurons;

			if (layer == (numHiddenLayers-1)){  // successor layer is output layer
				sucLayerPTR =  &(net._outputLayer);
				sucNumNeurons = net.getSizeOfOutputLayer();
			} else {  // successor layer is another hidden layer
				sucLayerPTR = &(net._hiddenLayer[layer+1]);
				sucNumNeurons = net.getSizeOfHiddenLayer(layer +1);
			}

			double weightedSumDelta = 0.0;

			for (std::size_t sucNeuron = 0; sucNeuron < sucNumNeurons; ++sucNeuron){
				double weigth = (*sucLayerPTR)[sucNeuron]->getWeight(neuron);
				weightedSumDelta += weigth * deltas[sucNeuron];
			}

			register double delta = derivateNet * weightedSumDelta;
			new_deltas[neuron] = delta;

#ifdef DEBUG
	std::cout << "		Training HiddenNeuron:" << neuron << " with delta " << delta << std::endl;
#endif
			// update Weights
			std::size_t numPreNeurons = net.getSizeOfHiddenLayer(layer - 1);
			std::valarray<double>& preLayerAct = data[layer-1].act;

			for (std::size_t weight = 0; weight < numPreNeurons; ++weight){
				register double diffWeigth = learningRate  * delta * preLayerAct[weight];

				actNeuron.addToWeight(weight, diffWeigth);
#ifdef DEBUG
	std::cout << "		Changed weigth by:" << diffWeigth << std::endl;
#endif
			}

		}

		deltas = new_deltas;

	}

	return data.back().act;


}


vec trainBackpropSample(Network& net,DataSet::TestCase& testCase, double learningRate){
	return trainBackpropSample(net, testCase._in, testCase._out, learningRate);
}

double trainBackpropEpoch(Network& net,DataSet& data, double learningRate, std::size_t trainSampleTimes){

	std::size_t trained = 0;
	for (auto& testCase : data._data){
		if (testCase.type == DataSet::TestCase::learn){
			for (std::size_t iteration = 0; iteration < trainSampleTimes; ++iteration ){
				trainBackpropSample(net,testCase,learningRate);
			}

			trained++;
		}
	}

	double diff = 0.0;
	std::size_t tested = 0;

	for (auto& testCase : data._data){
		if (testCase.type == DataSet::TestCase::test){
			auto ret = net.calculateTraining(testCase._in);
			diff += std::pow(calcAbsDiff(ret.back().act,testCase._out),2.0);
			//diff += calcAbsDiff(ret.back().act,testCase._out);
			tested++;
		}
	}
	diff = diff / double(tested);
	//std::cout << "Trained Epoch:  Trained "<< trained << " samples. Tested " << tested << " samples.";// << std::endl;

	return diff;
}

void Network::cleanLayer(Layer& layer) {
	for (Neuron* n : layer){
		delete n;
	}
	layer.clear();
}

std::ostream& operator <<(std::ostream& stream, const Network& network) {
	stream << "Printing Network:" << std::endl;
	stream << "Num. Inputs: "<< network._num_inputs
			<< "| Num. Outputs" << int(network.getSizeOfOutputLayer())
			<< "| Num Layers: "<< int(network.getNumHiddenLayers()) << std::endl;
	stream << "Printing Layers:" << std::endl;

	for (auto& layer : network._hiddenLayer) stream << layer << "--------------------------------------" << std::endl;
	stream << " Printing OutputLayer:" << std::endl << network._outputLayer << std::endl;
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const Layer& layer){
	stream << "\tPrinting Neuron Layer:" << std::endl;
	for (auto neuron : layer) stream << "\t\t" << *neuron;
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const Neuron& neuron){
	return stream << "Neuron [ ActFunc=\"" << getActFuncDesc(neuron._actFunc) << "\"]" <<std::endl; // TODO PROB ADD MORE
}
/*
int main(){
	try{
		DataSet data{3,3};

		const std::size_t maxCases = 100;

		std::random_device rd;
		std::mt19937 rand_dev{rd()};

		std::uniform_real_distribution<double> dist(0.0, 0.5);


		for (std::size_t numCase = 1; numCase < maxCases; ++numCase){
			vec sample_1_in{dist(rand_dev),dist(rand_dev)/2.0,dist(rand_dev)/2.0};
			//vec sample_1_out{sample_1_in[0],sample_1_in[2] + sample_1_in[1],sample_1_in[0]};
			vec sample_1_out{sample_1_in};

			std::cout << "Added Sample: in{" << sample_1_in[0] << "|" << sample_1_in[1] << "|" << sample_1_in[2] << "}  Out: {";
			std::cout <<  sample_1_out[0] << "|" <<  sample_1_out[1] << "|" <<  sample_1_out[2] << "}" << std::endl;

			data.addCase(sample_1_in,sample_1_out);
		}

		data.randomlyDistributeTestCases(0.5,0.25,0.25);

		Network test{3,3,10,3,true};

		test.initAllWeightsRandom(0.1,1.2);

		std::size_t iterations = 0;
		double learningRate = 0.9;
		double error = trainBackpropEpoch(test, data, 0.1, 10);
		std::cout << "Error:" << error << std::endl;
		while (error > 0.00001){
			double new_error = trainBackpropEpoch(test, data, learningRate, 10);
			//learningRate = (3.0 / std::pow(error,2.0));
			//if (new_error > error) learningRate *= 0.5;
			//if (new_error < error) learningRate *= 1.1;
			error = new_error;
			std::cout << "Iteration:"<< iterations++ << " | MSR:" << error << " | learning rate:" << learningRate << std::endl;

		}
		std::cout << "Trained the network " << iterations << " Times  Error: " << error << std::endl;
		// Iteration:1000 | Error:0.0350476
		/*
		std::size_t iterations = 0;
		double error = calcAbsDiff(trainBackpropSample(test,sample_1_in,sample_1_out,1.0),sample_1_out);
		std::cout << "Error:" << error << std::endl;
		while (error > 0.1){
			std::cout << "Iteration:"<< iterations << " | Error:" << error << std::endl;
			error = calcAbsDiff(trainBackpropSample(test,sample_1_in,sample_1_out,100.1),sample_1_out);
			++iterations;
		}
		std::cout << "Trained the network " << iterations << " Times  Error: " << error << std::endl;
		*/ /*
	}catch(std::exception& e ){
		std::cout << "catched exception dervide from std::exception : " << e.what() << std::endl;

	}catch(const char* e){
		std::cout << e << std::endl;

	}catch(...){
		std::cout << "chatched unknow exception" << std::endl;

	}

}

*/




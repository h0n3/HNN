/*****************************
 * @file	Neuron.h
 * @date	Jun 15, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef NEURON_H_
#define NEURON_H_

/*****************************
 * @brief	brief desc
 *	
 * long desc
 */

#include <iostream>
#include <random>
#include <valarray>
#include <math.h>

#include "DataDef.h"



enum ActivationFuncs{
	Identity = 0,
	Logistic = 1,
	NONE = 3

};

// Activation Funcs add new options here
double LogisticActivation(double in);
double LogisticActivationDerivate(double in);

double IdentityActivation(double in);
double IdentityActivationDerivate(double in);

class Neuron {
public:
	Neuron(size_t pre=0, ActivationFuncs activationFunction=Logistic);
	Neuron(const Neuron & n) = default;

	virtual ~Neuron();

	// Init weight group (if num_w != 0 the old weigths are deleted and num_w new weigths are generated)
	void initWeightsUniform(double val, size_t num_w = 0);
	void initWeightsRandom(double low, double high, size_t num_w = 0);

	void setWeights(weightLayer&& w);
	weightLayer& getWeights();

	double getWeight(std::size_t at);
	void setWeigth(std::size_t at, double weigth);
	void addToWeight(std::size_t at, double diff);


	double getNet(std::valarray<double>& in);
	double getOut(double net);

	ActivationFuncs getActFunc(){
		return _actFunc;
	}

	inline actFunc getActivationFunction(){
		switch(_actFunc){
		case Identity:{
			return &IdentityActivation;
		}
		case Logistic:{
			return &LogisticActivation;
		}
		}
		return nullptr;
	}

	inline actFunc getActivationFunctionDerivate(){
		switch(_actFunc){
		case Identity:{
			return &IdentityActivationDerivate;
		}
		case Logistic:{
			return &LogisticActivationDerivate;
		}
		}
		return nullptr;
	}

	friend std::ostream& operator <<(std::ostream& stream, const Neuron& neuron);

private:
	ActivationFuncs _actFunc;
	weightLayer _weights;
};

std::string getActFuncDesc(ActivationFuncs a);

bool isValidActivationFunction(ActivationFuncs a);


#endif /* NEURON_H_ */

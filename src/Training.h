/*****************************
 * @file	Training.h
 * @date	Jun 17, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef TRAINING_H_
#define TRAINING_H_

#include "Network.h"
#include "DataDef.h"
#include "DataSet.h"

vec trainBackpropSample(Network& net,DataSet::TestCase& testCase, double learningRate);
vec trainBackpropSample(Network& net, vec in, vec expected, double learningRate);

/******************************************
 * Trains all Data that is marked as train/validate once
 * @param net Network to train
 * @param data Dataset containing the samples
 * @param learningRate The learning rate to be applied
 */
double trainBackpropEpoch(Network& net,DataSet& data, double learningRate, std::size_t trainSampleTimes = 1);


#endif /* TRAINING_H_ */

/*****************************
 * @file	DataSet.h
 * @date	Jun 17, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef DATASET_H_
#define DATASET_H_

#include "DataDef.h"
#include "Network.h"

/*****************************
 * @brief	brief desc
 *	
 * long desc
 */


class DataSet {
public:
	struct TestCase{

		enum Type{
			learn,
			validate,
			test
		};

		Type type;

		vec _in;
		vec _out;

		TestCase(vec&& in, vec&& out):_in{in},_out{out}{} // only move cuz it makes no sense to add the case and keep a copy of it

	};

	DataSet(std::size_t in, std::size_t out);
	virtual ~DataSet();

	// add cases
	void addCase(TestCase& testCase);
	void addCase(TestCase&& testCase); // TODO do i have to manually define the move constructor to achieve a speed gain ?
	void addCase(vec in, vec out);


	// Distribute TesCases group TODO add doc
	void randomlyDistributeTestCases(double learn, double validate, double test);

	// Statistics group
	double calcRMS(Net& net);
	double calcRMSTest(Net& net);

	// TODO save/load
	bool loadDataFromCSV(std::string filename);

	std::vector<TestCase> _data;

private:

	std::size_t _numInputs;
	std::size_t _numOutputs;


};

#endif /* DATASET_H_ */

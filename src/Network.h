/*****************************
 * @file	Network.h
 * @date	Jun 15, 2017
 * @author	Hans Hirschberg
 * @brief	brief description
 * 
 * full description
 */
#ifndef NETWORK_H_
#define NETWORK_H_

#include <iostream>

#include <vector>
#include <array>
#include <valarray>
#include <memory>

#include "DataDef.h"
#include "Neuron.h"
#include "DataSet.h"
//#include "Training.h"


/*****************************
 * @brief	brief desc
 *	
 * long desc
 */



class Network {
public:

	struct TrainingDataLayer{
		TrainingDataLayer()
		:net{0},act{0}{}
		TrainingDataLayer(const std::size_t size)
		:net(size),act(size) {}

		std::valarray<double> net;
		std::valarray<double> act;
	};
	Network();

	// Constructor TODO add doc
	Network(std::size_t inputs, std::size_t outputs, std::size_t hiddenLayer=1, std::size_t sizeLayer=5, bool connect=false);

	// TODO add constructor with fixed layer sizes
	//Network(std::size_t inputs, std::size_t outputs, std::initializer_list<std::size_t> hiddenLayer, bool connect=false);

	virtual ~Network();

	// Get size funcs to make code more readable
	std::size_t getSizeOfInputLayer() const;
	void setSizeOfInputLayer(std::size_t size);

	std::size_t getNumHiddenLayers() const;
	std::size_t getSizeOfHiddenLayer(std::size_t layer) const;

	std::size_t getSizeOfOutputLayer() const;

	// Init weigths
	void initAllWeightsRandom(double low, double high);
	void initAllWeightsUniform(double init);

	// Access single neuron group TODO add doc
	Neuron* getHiddenNeuron(std::size_t layer, std::size_t neuron);
	void setHiddenNeuron(std::size_t layer, std::size_t neuron, Neuron* n);

	Neuron* getOutputNeuron(std::size_t neuron);
	void setOutputNeuron(std::size_t neuron, Neuron* n);

	// Access
	weightLayer& getHiddenWeigths(std::size_t layer, std::size_t neuron);
	void setHiddenWeights(std::size_t layer, std::size_t neuron, weightLayer&& weights);

	weightLayer& getOutputWeigths(std::size_t neuron);
	void setOutputWeights(std::size_t neuron, weightLayer&& weights);

	// Change Layer group TODO add doc
	void setHiddenLayer(Layer&& layer , std::size_t at=-1);
	void setHiddenLayer(std::size_t at, size_t num_neurons, const Neuron& bluePrint);

	void addHiddenLayer(Layer&& layer);
	void addHiddenLayer(size_t num_neurons, const Neuron& bluePrint);

	void setOutputLayer(Layer&& layer);

	//void removeLayer(std::size_t at);

	std::vector<TrainingDataLayer> calculateTraining(vec& in);

	vec calculate(vec in);

	//std::vector<double>&& calculateInputLayer(vec& in, std::vector<double>* net = nullptr);
	//std::vector<double>&& calculateLayer(std::size_t layer, std::vector<double> pre, std::vector<double>* net = nullptr);
	//vec&& calculateOutputLayer(std::vector<double> pre, vec* net= nullptr);


	//friend vec trainBackpropSample(Network& net,DataSet::TestCase& testCase, double learningRate);
	friend vec trainBackpropSample(Network& net, vec in, vec expected, double learningRate);


	// Label Funcs
	void setLabelsIn(std::vector<std::string>&& lbls);
	void setLabelsOut(std::vector<std::string>&& lbls);

	void setLabelIn(std::size_t at, std::string lbl);
	void setLabelOut(std::size_t at, std::string lbl);

	std::string getLabelIn(std::size_t at) const;
	std::string getLabelOut(std::size_t at) const;

	// DEBUG FUNCS
	friend class NetworkListWidget;

	friend std::ostream& operator <<(std::ostream& stream, const Network& matrix);

private:
	void cleanLayer(Layer& layer);


	std::size_t _num_inputs;
	Net _hiddenLayer;
	Layer _outputLayer;

	//Label
	std::vector<std::string> _lblIn;
	std::vector<std::string> _lblOut;

};

// HELPER FUNCS

double calcAbsDiff(vec v1, vec v2);


std::ostream& operator <<(std::ostream& stream, const Network& matrix);
std::ostream& operator <<(std::ostream& stream, const Layer& layer);
std::ostream& operator <<(std::ostream& stream, const Neuron& neuron);


#endif /* NETWORK_H_ */
